package me.spideusz.particles;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.spideusz.particles.commands.ParticlesMenu;
import me.spideusz.particles.listeners.ParticlesMenuClick;
import me.spideusz.particles.runnables.ParticleSpawner;

public class Particles extends JavaPlugin {
	
	private static Particles main;
	public Particles(){ main = this; }
	public static Particles getMain() { return main; }
	
	@Override
	public void onEnable() {
		
		// Commands
		this.getCommand("pmenu").setExecutor(new ParticlesMenu());
		
		// Listeners
		PluginManager pManager = Bukkit.getPluginManager();

		pManager.registerEvents(new ParticlesMenuClick(), this);
		
		// Config
		saveDefaultConfig();
		
		// Runnables
		ParticleSpawner.start();
	}
	
	@Override
	public void onDisable() {
		
		// TODO
		
	}
	
}
