package me.spideusz.particles.listeners;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import me.spideusz.particles.ColourTranslator;
import me.spideusz.particles.Particles;
import me.spideusz.particles.effects.ParticlesStorage;

public class ParticlesMenuClick implements Listener {
	
	@EventHandler
	public void particlesMenuOpenEvent(InventoryClickEvent event) {
		if(event.getInventory().getTitle().equalsIgnoreCase(ColourTranslator.translate(Particles.getMain().getConfig().getString("menu-title")))) {
			event.setCancelled(true);
			if(event.getCurrentItem() == null || event.getCurrentItem().getType().equals(Material.AIR)) {
				return;
			} else {
				ItemMeta itemmeta = event.getCurrentItem().getItemMeta();
				Player player = (Player) event.getWhoClicked();
				if(itemmeta.getDisplayName().equalsIgnoreCase(ColourTranslator.translate(Particles.getMain().getConfig().getString("note-title")))) {
					ParticlesStorage.addParticle(player, Effect.NOTE);
				}
				if(itemmeta.getDisplayName().equalsIgnoreCase(ColourTranslator.translate(Particles.getMain().getConfig().getString("flame-title")))) {
					ParticlesStorage.addParticle(player, Effect.LAVADRIP);
				}
				if(itemmeta.getDisplayName().equalsIgnoreCase(ColourTranslator.translate(Particles.getMain().getConfig().getString("waterbubble-title")))) {
					ParticlesStorage.addParticle(player, Effect.WATERDRIP);
				}
				if(itemmeta.getDisplayName().equalsIgnoreCase(ColourTranslator.translate(Particles.getMain().getConfig().getString("remove-effect-title")))) {
					ParticlesStorage.removeParticle(player);
				}
			}
		}
	}
	
}
