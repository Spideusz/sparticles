package me.spideusz.particles.effects;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Effect;
import org.bukkit.entity.Player;

import me.spideusz.particles.ColourTranslator;
import me.spideusz.particles.Particles;

public class ParticlesStorage {

	private static Set<ParticleManager> particlesstorage = new HashSet<>();
	
	public static void addParticle(Player player, Effect effect) {
		for(ParticleManager particlemanager : particlesstorage) {
			if(particlemanager.getPlayerName().contains(player.getName())) {
				particlesstorage.remove(particlemanager);
			}
		}
		
		particlesstorage.add(new ParticleManager(player.getName(), effect));
	}
	
	public static void removeParticle(Player player) {
		for(ParticleManager particlemanager : particlesstorage) {
			if(particlemanager.getPlayerName().contains(player.getName())) {
				particlesstorage.remove(particlemanager);
			}
		}
		
		player.sendMessage(ColourTranslator.translate(Particles.getMain().getConfig().getString("removed-effect-message")));
	}
	
	
	
	public static Set<ParticleManager> getEffectsStorage(){
		return particlesstorage;
	}
	
}
