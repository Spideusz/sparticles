package me.spideusz.particles.effects;

import org.bukkit.Effect;

public class ParticleManager {

	private String playername;
	private Effect effect;
	
	public ParticleManager(String playername, Effect effect) {
		this.playername = playername;
		this.effect = effect;
	}
	
	public Effect getEffect() {
		return effect;
	}
	
	public String getPlayerName() {
		return playername;
	}
	
}
