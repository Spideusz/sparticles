package me.spideusz.particles.utils;

import java.util.ArrayList;

import me.spideusz.particles.ColourTranslator;

public class LoreCreator {

	public static ArrayList<String> create(String arg0){
		ArrayList<String> lore = new ArrayList<>();
		lore.add(ColourTranslator.translate(arg0));
		return lore;
	}
	
	public static ArrayList<String> create(String arg0, String arg1){
		ArrayList<String> lore = new ArrayList<>();
		
		lore.add(ColourTranslator.translate(arg0));
		lore.add(ColourTranslator.translate(arg1));
		
		return lore;
	}
	
	public static ArrayList<String> create(String arg0, String arg1, String arg2){
		ArrayList<String> lore = new ArrayList<>();
		
		lore.add(ColourTranslator.translate(arg0));
		lore.add(ColourTranslator.translate(arg1));
		lore.add(ColourTranslator.translate(arg2));
		
		return lore;
	}
	
	public static ArrayList<String> create(String arg0, String arg1, String arg2, String arg3){
		ArrayList<String> lore = new ArrayList<>();
		
		lore.add(ColourTranslator.translate(arg0));
		lore.add(ColourTranslator.translate(arg1));
		lore.add(ColourTranslator.translate(arg2));
		lore.add(ColourTranslator.translate(arg3));
		
		return lore;
	}
	
	public static ArrayList<String> create(String arg0, String arg1, String arg2, String arg3, String arg4){
		ArrayList<String> lore = new ArrayList<>();
		
		lore.add(ColourTranslator.translate(arg0));
		lore.add(ColourTranslator.translate(arg1));
		lore.add(ColourTranslator.translate(arg2));
		lore.add(ColourTranslator.translate(arg3));
		lore.add(ColourTranslator.translate(arg4));
		
		return lore;
	}
}
