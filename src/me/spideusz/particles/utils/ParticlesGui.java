package me.spideusz.particles.utils;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import me.spideusz.particles.Particles;

public class ParticlesGui {

	public static void set(Player player) {
		Inventory inventory = InventoryCreator.create(Particles.getMain().getConfig().getString("menu-title"), 2);
		
		inventory.setItem(1, ItemCreator.create(Material.SLIME_BALL, Particles.getMain().getConfig().getString("note-title"), 1, null));
		inventory.setItem(3, ItemCreator.create(Material.FIREBALL, Particles.getMain().getConfig().getString("flame-title"), 1, null));
		inventory.setItem(5, ItemCreator.create(Material.WATER_BUCKET, Particles.getMain().getConfig().getString("waterbubble-title"), 1, null));
		inventory.setItem(17, ItemCreator.create(Material.PAPER, Particles.getMain().getConfig().getString("remove-effect-title"), 1, null));
		
		player.openInventory(inventory);
	}
	
}
