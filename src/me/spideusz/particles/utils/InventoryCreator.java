package me.spideusz.particles.utils;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;

import me.spideusz.particles.ColourTranslator;

public class InventoryCreator {

	public static Inventory create(String title, int rows) {
		Inventory inventory = Bukkit.createInventory(null, (rows * 9), ColourTranslator.translate(title));
		
		return inventory;
	}
	
}
