package me.spideusz.particles.utils;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.spideusz.particles.ColourTranslator;

public class ItemCreator {

	public static ItemStack create(Material material, String name, int amount, ArrayList<String> lore) {
		ItemStack item = new ItemStack(material);
		ItemMeta itemmeta = item.getItemMeta();
		
		itemmeta.setDisplayName(ColourTranslator.translate(name));
		itemmeta.setLore(lore);
		item.setItemMeta(itemmeta);
		item.setAmount(amount);
		
		return item;
	}
	
}
