package me.spideusz.particles.runnables;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.spideusz.particles.Particles;
import me.spideusz.particles.effects.ParticleManager;
import me.spideusz.particles.effects.ParticlesStorage;

public class ParticleSpawner {

	public static void start() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Particles.getMain(), ()->{
			
			if(!(ParticlesStorage.getEffectsStorage().isEmpty())) {
				for(ParticleManager particlemanager : ParticlesStorage.getEffectsStorage()) {
					if(Bukkit.getPlayer(particlemanager.getPlayerName()) != null) {
						Player player = Bukkit.getPlayer(particlemanager.getPlayerName());
						Bukkit.getWorld(player.getWorld().getName()).playEffect(player.getLocation().add(player.getLocation().getDirection().multiply(-1.25)).add(0, 0.5, 0), particlemanager.getEffect(), 5);
					} else ParticlesStorage.getEffectsStorage().remove(particlemanager);
				}
			}
			
		}, 0, 2);
	}

	
}
