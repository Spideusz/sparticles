package me.spideusz.particles.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.spideusz.particles.utils.ParticlesGui;

public class ParticlesMenu implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			ParticlesGui.set(player);
		}
		return false;
	}
	
}
